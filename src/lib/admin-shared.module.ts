import { CommonModule } from "@angular/common";
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from "@angular/core";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { environment } from "./environments/environment";
import { ADMIN_LOCALES } from "./i18n";
import { AdminService } from "./admin.service";

@NgModule({
    imports: [
        TranslateModule,
        CommonModule,
    ],
    declarations: [
    ],
    exports: [
    ]
  })
  export class AdminSharedModule {
    constructor( @Optional() @SkipSelf() parentModule: AdminSharedModule, private translateService: TranslateService) {
      this.ngOnInit();
    }
  
    static forRoot(): ModuleWithProviders {
      return {
        ngModule: AdminSharedModule,
        providers: [
          AdminService
        ]
      };
    }
  
    // tslint:disable-next-line:use-life-cycle-interface use-lifecycle-interface
    ngOnInit() {
      environment.languages.forEach( (language) => {
        if (ADMIN_LOCALES.hasOwnProperty(language)) {
          this.translateService.setTranslation(language, ADMIN_LOCALES[language], true);
        }
      });
    }
  
  }