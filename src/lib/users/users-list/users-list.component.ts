import { Component, OnInit } from '@angular/core';
import {UserListConfiguration} from '../../../assets/tables/users/list';
import { AdvancedTableConfiguration } from '@universis/tables';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: []
})
export class UsersListComponent implements OnInit {

  public readonly tableConfiguration:any = AdvancedTableConfiguration.cast(UserListConfiguration);
  public filter: any = {};

  constructor() { }

  ngOnInit() {

  }

}
