import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {AdvancedFormComponent} from "@universis/forms";
import {AppEventService} from "@universis/common";

@Component({
  selector: 'lib-user-edit',
  templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit {
  public user: any;
  public userId: any;
  @Input() data: any;
  @Input() src: any;
  @ViewChild('form') form: AdvancedFormComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.userId = this._activatedRoute.parent.snapshot.params.id
    }

    this.user = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'User') {
        // reload request
        if (event.target.id == this.userId) {
          this.user = event.target;

          if (this.user) {
            this.src = `Users/edit`;
            this.data = this.user;
          }
        }
      }
    });
  }

  onCompletedSubmission($event: any) {

  }
}
