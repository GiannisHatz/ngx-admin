import {Component, OnInit, ViewChild} from '@angular/core';
import {AdvancedTableComponent, AdvancedTableConfiguration} from "@universis/tables";
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {GroupsListConfiguration} from '../../../../assets/tables/groups/list';
import {AppEventService} from "@universis/common";

@Component({
  selector: 'lib-user-groups',
  templateUrl: './user-groups.component.html'
})
export class UserGroupsComponent implements OnInit {

  @ViewChild('groups') groups: AdvancedTableComponent;
  public filter: any = {};
  public user: any;
  public userId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) { }

  async ngOnInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.userId = this._activatedRoute.parent.snapshot.params.id
    }

    this.user = this._appEvent.change.subscribe(async event => {
      if (event && event.target && event.model === 'User') {
        // reload request
        if (event.target.id == this.userId) {
          this.user = event.target;

          if (this.user) {
            this.groups.config = AdvancedTableConfiguration.cast(GroupsListConfiguration);
            this.groups.config.model = `Users/${this.user.id}/groups`;
          }
        }
      }
    });
  }
}
