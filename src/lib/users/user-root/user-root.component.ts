import {Component, EventEmitter, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {AppEventService} from "@universis/common";

@Component({
  selector: 'lib-user-root',
  templateUrl: './user-root.component.html'
})
export class UserRootComponent implements OnInit {
  public user: any;
  public isCreate = false;
  public config: any;
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) { }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.params.id) {
      this.user = await this._context.model('users')
        .where('id').equal(this._activatedRoute.snapshot.params.id)
        .getItem();
      this._appEvent.change.next({
        model: 'User',
        target: this.user
      });
    }
  }

}
