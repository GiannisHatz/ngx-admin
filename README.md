## @universis/ngx-admin

Universis Admin client library for angular

### Installation

    npm i @universis/ngx-admin

### Usage 

Import `AdminModule` in app modules

    ...
    import { AdminModule } from '@universis/ngx-admin';

    @NgModule({
    declarations: [
        ...
    ],
    imports: [
        ...
        AdminModule
    ],
        providers: [
            ...
        ],
        bootstrap: [ AppComponent ]
    })
    export class AppModule {

    }

### Development

This project is intended to be used as a part of an angular cli project. 

- Add this project as git submodule

        git submodule add https://gitlab.com/universis/ngx-admin.git packages/ngx-admin

- Edit angular.json and add `ngx-admin` configuration under `projects` node:

        "ngx-admin": {
            "root": "packages/ngx-admin",
            "sourceRoot": "packages/ngx-admin/src",
            "projectType": "library",
            "prefix": "universis",
            "architect": {
                "build": {
                    "builder": "@angular-devkit/build-ng-packagr:build",
                    "options": {
                        "tsConfig": "packages/ngx-admin/tsconfig.lib.json",
                        "project": "packages/ngx-admin/ng-package.json"
                    }
                },
                "test": {
                "builder": "@angular-devkit/build-angular:karma",
                "options": {
                    "main": "packages/ngx-admin/src/test.ts",
                    "tsConfig": "packages/ngx-admin/tsconfig.spec.json",
                    "karmaConfig": "packages/ngx-admin/karma.conf.js"
                }
                },
                "lint": {
                "builder": "@angular-devkit/build-angular:tslint",
                "options": {
                    "tsConfig": [
                        "packages/ngx-admin/tsconfig.lib.json",
                        "packages/ngx-admin/tsconfig.spec.json"
                    ],
                    "exclude": [
                    "**/node_modules/**"
                    ]
                }
            }
        }
        }

- Import `@universis/ngx-admin` in tsconfig.json#paths

        {
            ...
            "compilerOptions": {
                ...
                "paths": {
                    ...
                    "@universis/ngx-admin": [
                        "packages/ngx-admin/src/public_api"
                    ]
                }
            }
        }
